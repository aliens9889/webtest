<?php 
session_start(); 
if(isset($_SESSION["usuario"]) == false){
	header("Location:index.php");	
}
?>
<!doctype html>
 <html>

   <head>
      <meta charset="utf-8"/>  
	  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	  <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale=1.0"/>
	  <!--Site Properties-->
	  <title>iDents</title>
	  <!-- css -->
		
		<link rel="stylesheet" href="assets/css/base-cliente.css" />
		<link rel="stylesheet" href="assets/css/menu-cliente.css" />
		<link rel="stylesheet" href="components/simpleGrid/simple-grid.min.css" />
		
		<link rel="stylesheet" href="assets/css/input.css" />
		<link rel="stylesheet" href="assets/css/buttons.css" />
		
		<link rel="stylesheet" href="components/pretty-dropdowns/css/prettydropdowns.css" />
		
		<!-- js -->
		<script src="assets/js/jquery-1.9.1.min.js"></script>
		<script src="assets/js/modernizr.custom.js"></script>
		
		<script src="components/pretty-dropdowns/js/jquery.prettydropdowns.js"></script>
	  <!--Site Properties-->      
   </head>

   <style>
   .panel_escritorio{
	   padding:20px;	   
   }
   
   .panel_escritorio h2{
	   color:#fff;   
   }
   
   
   .panel_indigo{
	   background:#3F51B5;
       color:#fff;	   
   }
   .panel_blue{
	   background:#2196F3;
       color:#fff;	   
   }
   .panel_green{
	   background:#009688;
       color:#fff;	   
   }
   </style>
   
   <script>
    $(document).ready(function(){
        $("#nav-mobile").html($("#nav-main").html());
		
		
        $("#nav-trigger div").click(function(){
		
			if ($("nav#nav-mobile").hasClass("expanded")) {
				$("nav#nav-mobile").removeClass("expanded");

				$("#nav-mobile").css("display","none");
				$("nav#nav-mobile ul").css("display","none");
							
			}else{
				$("nav#nav-mobile").addClass("expanded");

				$("#nav-mobile").css("display","block");
				$("nav#nav-mobile ul").css("display","block");
			}
		
            /*if ($("nav#nav-mobile ul").hasClass("expanded")) {
                $("nav#nav-mobile ul.expanded").removeClass("expanded")//.slideUp(250);
                //$(this).removeClass("open");
				
				$("nav#nav-mobile").css("visibility","hidden");
				
            } else {
                $("nav#nav-mobile ul").addClass("expanded")//.slideDown(250);
                //$(this).addClass("open");
				
				$("nav#nav-mobile").css("visibility","visible");
				
            }*/
        });
		
		var mediaquery = window.matchMedia("(max-width: 900px)");
			
		function handleOrientationChange(mediaquery) {
		  if (mediaquery.matches) {
			// mediaquery dentro de 900 - Movile
			//alert("mediaquery es 900");
			
		  } else {
			// mediaquery fuera de 900 - No Movile
			//alert("mediaquery no es 900");
			$("nav#nav-mobile").removeClass("expanded");
			$("#nav-mobile").css("display","none");
			$("nav#nav-mobile ul").css("display","none");
		  }
		}
		mediaquery.addListener(handleOrientationChange);
		
		
    });
	</script>
   
   <body>
	 
	 <div id="wrapper">

		<?php
		if($_SESSION["rol"] == "ADMINISTRADOR"){
			require "menu_administrador.php"; 
		}
				
		if($_SESSION["rol"] == "VENDEDOR"){
			require "menu_vendedor.php"; 
		}
		?>	
			
<div id="main">
			
		<div class="container">		
				
		
			
					<h1><b>Confirmar pago</b></h1>
			
			<br>
			
			<h2>Rafael Jose Garcia Suarez</h2>
			<h3>20.880.061</h3>
			
			<br>
			
				<div class="row">
							<div class="col-5">
								<select name="qty" class="pretty" onchange="alert('You selected ' + value)">
									<option value="1">BanPlus (Banco Nacional de Credito)</option>
								</select>
							</div>
							<div class="col-5">
								<input type="text" placeholder="Numero del Deposito/Transferencia">
							</div>
							<div class="col-2">
								<input type="text" placeholder="Monto">
							</div>
				</div>
				
			<button class="ff_btn btn_blue btn_medium">Confirmar pago</button>
			
			</div>
			
		</div><!-- #main -->


		<footer>
		</footer><!-- /footer -->
	</div><!-- /#wrapper -->
	 
	 <script>
	 $(document).ready(function(){
		 $('select').prettyDropdown();		 
	 })
	 </script>
	 
   </body>

 </html>