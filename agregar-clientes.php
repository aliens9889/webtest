<?php 
session_start(); 
if(isset($_SESSION["usuario"]) == false){
	header("Location:index.php");	
}
?>
<!doctype html>
 <html>

   <head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
        
	  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	  <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale=1.0"/>
	  <!--Site Properties-->
	  <title>Clientes</title>
	  <!-- css -->
		
		<link rel="stylesheet" href="assets/css/base-cliente.css" />
		<link rel="stylesheet" href="assets/css/menu-cliente.css" />
		<link rel="stylesheet" href="components/simpleGrid/simple-grid.min.css" />
		
		<link rel="stylesheet" href="components/table/cyrfB.css" />
		<link rel="stylesheet" href="assets/css/input.css" />
		<link rel="stylesheet" href="assets/css/buttons.css" />
		
		<!-- js -->
		<script src="assets/js/jquery-1.9.1.min.js"></script>
		<script src="assets/js/modernizr.custom.js"></script>
		
		<script src="assets/js/main.js"></script>
	  <!--Site Properties-->      
   </head>
   
   <body>
	 
	 <div id="wrapper">

		<?php 
if($_SESSION["rol"] == "ADMINISTRADOR"){
			require "menu_administrador.php"; 
		}
				
		if($_SESSION["rol"] == "VENDEDOR"){
			require "menu_vendedor.php"; 
		}

		?>	
			
<div id="main">
			
		<div class="container">		
				
				
				<?php 
				
					$codigo = "";
					$nombres = "";
					$apellidos = "";
					$rif= "";
					$direccion_fiscal= "";
					$direccion_entrega= "";
					$telefono_movil= "";
					$telefono_habitacion= "";
					$fax = "";
					$zona= "";
				
				
				if(isset($_GET)){
					$codigo = $_GET["codigo"];
					$nombres = $_GET["nombres"];
					$apellidos = $_GET["apellidos"];
					$rif= $_GET["rif"];
					$direccion_fiscal= $_GET["direccion_fiscal"];
					$direccion_entrega= $_GET["direccion_entrega"];
					$telefono_movil= $_GET["telefono_movil"];
					$telefono_habitacion= $_GET["telefono_habitacion"];
					$fax = $_GET["fax"];
					$zona= $_GET["zona"];
				}
				
				
				?>
				
				
				
		<form id="formulario" action="modulos/agregar-cliente.php" method="POST">
			
					<h1>Agregar nuevo cliente</h1>
			
			<br>	<!-----<br>
			<h3>Informaci贸n de cuenta</h3>
			
					<div class="row">
					
					
							<div class="col-6">
							  <input type="text" name ="usuario" value="<?php echo $usuario; ?>" placeholder="Usuario">
							</div>
							<div class="col-6">
							  <input type="text" name ="contrasena" placeholder="Contraseña">
							</div>
					</div>
					----->
			
					
					<h3>Informaci贸n de perfil</h3>
					<input type="hidden" name="codigo" value="<?php echo $codigo; ?>">
					<div class="row">
							<div class="col-6">
							  <input type="text" name ="nombres" value="<?php echo $nombres; ?>" placeholder="Nombres">
							</div>
							<div class="col-6">
							  <input type="text" name ="apellidos" value="<?php echo $apellidos; ?>" placeholder="Apellidos">
							</div>
							
							
							<div class="col-6">
							  <input type="text" name ="rif" value="<?php echo $rif; ?>" placeholder="Rif">
							</div>
							<div class="col-6">
							  <input type="text" name ="direccion_fiscal" value="<?php echo $direccion_fiscal; ?>" placeholder="Direccion fiscal">
							</div>
							
							<div class="col-12">
							  <input type="text" name ="direccion_entrega"  value="<?php echo $direccion_entrega; ?>" placeholder="Direccion de entrega">
							</div>
							
							
							<div class="col-6">
							  <input type="text" name ="telefono_movil" value="<?php echo $telefono_movil; ?>" placeholder="Telefono movil">
							</div>
							<div class="col-6">
							  <input type="text" name ="telefono_habitacion" value="<?php echo $telefono_habitacion; ?>" placeholder="Telefono habitacion">
							</div>
							
							<div class="col-6">
							  <input type="text" name ="fax"  value="<?php echo $fax; ?>" placeholder="Fax">
							</div>
							
							
							<div class="col-6">
							  <input type="text" name ="zona"  value="<?php echo $zona; ?>" placeholder="Zona">
							</div>
					</div>
			

</br>			

<a id="entrar" class="ff_btn btn_blue btn_medium" href="#">Guardar</a>

<?php  
if(isset($_GET["codigo"])){
	print '<a id="eliminar" class="ff_btn btn_red btn_medium" href="modulos/eliminar_cliente.php?codigo='.$_GET["codigo"].'">Eliminar</a>';
}
?>


			</form>
			</div>
			
		</div><!-- #main -->


		<footer>
		</footer><!-- /footer -->
	</div><!-- /#wrapper -->
	 
	 <script>
	 $(document).ready(function(a){
	 
		 $("#entrar").click(function(a){
			 a.preventDefault();
			 a.stopPropagation();	 			 
			 
			 $("#formulario").submit();
			 
		 })
		 	 
		 
	 })
	 </script>
	 
   </body>

 </html>