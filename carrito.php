<?php 
session_start(); 
if(isset($_SESSION["usuario"]) == false){
	header("Location:index.php");	
}else if($_SESSION["codigo_cliente"] == "null"){
	header("Location:clientes.php");	
}
?>
<!doctype html>
 <html>

   <head><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
        
	  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	  <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale=1.0"/>
	  <!--Site Properties-->
	  <title>Carrito</title>
	  <!-- css -->
		
		<link rel="stylesheet" href="assets/css/base-cliente.css" />
		<link rel="stylesheet" href="assets/css/menu-cliente.css" />
		<link rel="stylesheet" href="components/simpleGrid/simple-grid.min.css" />
		
		<link rel="stylesheet" href="assets/css/input.css" />
		<link rel="stylesheet" href="assets/css/buttons.css" />
		
		<link rel="stylesheet" href="components/table/cyrfB.css" />
		
		<!-- js -->
		<script src="assets/js/jquery-1.9.1.min.js"></script>
		<script src="assets/js/modernizr.custom.js"></script>
		
		<script src="assets/js/main.js"></script>
	  <!--Site Properties-->      
   </head>
   
   <body>
	 
	 <div id="wrapper">

		<?php
		if($_SESSION["rol"] == "ADMINISTRADOR"){
			require "menu_administrador.php"; 
		}
				
		if($_SESSION["rol"] == "VENDEDOR"){
			require "menu_vendedor.php"; 
		}
		?>	
			
<div id="main">
<form action="modulos/confirmar-venta.php" method="POST">

<input type="hidden" name="codigo_cliente" value="<?php print $_SESSION["codigo_cliente"]; ?>">

		<div class="container">		
					<h1>Carrito de compra</h1>
					<h3>Productos añadidos al carro : </h3><label id="cantidad_producto_carro"><?php 
					$da = json_decode($_SESSION["carrito"]);	
					$i = 0;
					foreach ($da as &$valor) {
						$i += $valor[3];
					}
					print $i;
					?></label>
			<br>
			<table class="table-fill">
<thead>
<tr>
<th class="text-left">Detalles</th>
<th class="text-left">Cantidad</th>
</tr>
</thead>
<tbody class="table-hover">
			<?php 
			$da = json_decode($_SESSION["carrito"]);
			(float)$costo = 0;
			foreach ($da as &$valor) {
			
			$costo = $costo + ((float)$valor[2] * (int)$valor[3]);
			
				print '

<tr>
<td class="text-left">'.$valor[1].' con un costo de '.$valor[2].'</td>
<td class="text-left">'.$valor[3].'</td>
</tr>
';
				
			}
			
			
			?>
</tbody>
</table>

<br>
<h3>Total a pagar : <?php print $costo; ?></h3>
<br>
<h3>Cliente : <?php print $_SESSION["cliente"]; ?></h3>
</br>
</br>
<button type="submit" id="confirmar-venta" class="ff_btn btn_red btn_medium">Confirmar venta</button>
<button id="entrar" class="ff_btn btn_blue btn_medium" href="modulos/vaciar-carrito.php">Vaciar carrito</button>
</form>
</br>			
			
			</div>
			
		</div><!-- #main -->


		<footer>
		</footer><!-- /footer -->
	</div><!-- /#wrapper -->

	 
   </body>

 </html>