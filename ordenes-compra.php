<?php 
session_start(); 
if(isset($_SESSION["usuario"]) == false){
	header("Location:index.php");	
}
?>
<!doctype html>
 <html>

   <head>
      <meta charset="utf-8"/>  
	  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	  <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale=1.0"/>
	  <!--Site Properties-->
	  <title>Ordenes de compra</title>
	  <!-- css -->
		
		<link rel="stylesheet" href="assets/css/base-cliente.css" />
		<link rel="stylesheet" href="assets/css/menu-cliente.css" />
		<link rel="stylesheet" href="components/simpleGrid/simple-grid.min.css" />
		
		<link rel="stylesheet" href="assets/css/input.css" />
		<link rel="stylesheet" href="assets/css/buttons.css" />
		
		<link rel="stylesheet" href="components/pretty-dropdowns/css/prettydropdowns.css" />
		
		<!-- js -->
		<script src="assets/js/jquery-1.9.1.min.js"></script>
		<script src="assets/js/modernizr.custom.js"></script>
		
		<script src="assets/js/main.js"></script>
		
		<script src="components/pretty-dropdowns/js/jquery.prettydropdowns.js"></script>
		
	  <!--Site Properties-->      
   </head>
   
   <body>
	 
	 <div id="wrapper">

		<?php 
if($_SESSION["rol"] == "ADMINISTRADOR"){
			require "menu_administrador.php"; 
		}
				
		if($_SESSION["rol"] == "VENDEDOR"){
			require "menu_vendedor.php"; 
		}
		?>	
			
<div id="main">
			
		<div class="container">		
				
		
			<form id="formulario" action="modulos/agregar-cheque-devuelto.php" method="POST">
					<h1>Tus ordenes de compra</h1>
					
								</br>
								
								<?php require_once "modulos/lista-ordenes-compra.php"; ?>
							
		
	
</br>			

</form>
			
			</div>
			
		</div><!-- #main -->


		<footer>
		</footer><!-- /footer -->
	</div><!-- /#wrapper -->
	 
	 <script>
	 $(document).ready(function(){
		 
		 $("#guardar").click(function(){
			$("#formulario").submit();
		 })
		 
		 $('.modificar-productos').click(function(e){
		 
			$.post( "modulos/modificar-existencia.php", { codigo: $(this).attr("id"), existencia: $("#txt_" + $(this).attr("id")).val() }, function( data ) {
				console.log(data);
				alert("Existencia modificada");
			})
			 
		 })	

$('select').prettyDropdown();	
		 
	 })
	 </script>
	 
   </body>

 </html>