<?php 
session_start(); 
if(isset($_SESSION["usuario"]) == false){
	header("Location:index.php");	
}
?>
<!doctype html>
 <html>

   <head>
      <meta charset="utf-8"/>  
	  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	  <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale=1.0"/>
	  <!--Site Properties-->
	  <title>Clientes</title>
	  <!-- css -->
		
		<link rel="stylesheet" href="assets/css/base-cliente.css" />
		<link rel="stylesheet" href="assets/css/menu-cliente.css" />
		<link rel="stylesheet" href="components/simpleGrid/simple-grid.min.css" />
		
		<link rel="stylesheet" href="components/table/cyrfB.css" />
		<link rel="stylesheet" href="assets/css/input.css" />
		<link rel="stylesheet" href="assets/css/buttons.css" />
		
		<!-- js -->
		<script src="assets/js/jquery-1.9.1.min.js"></script>
		<script src="assets/js/modernizr.custom.js"></script>
		
		<script src="assets/js/main.js"></script>
	  <!--Site Properties-->      
   </head>
   
   <body>
	 
	 <div id="wrapper">

		<?php 
if($_SESSION["rol"] == "ADMINISTRADOR"){
			require "menu_administrador.php"; 
		}
				
		if($_SESSION["rol"] == "VENDEDOR"){
			require "menu_vendedor.php"; 
		}
		?>	
			
<div id="main">
			
		<div class="container">		
				
		
			
					<h1>Clientes</h1>
			<input type="hidden" id="rol" value="<?php print $_SESSION["rol"]; ?>">
			
			<br>
			<input type="text" id="pista" name="pista" placeholder="Que quieres buscar?" />
			
			</br>
		
<div id="contenido"></div>

</br>			

<a id="entrar" class="ff_btn btn_blue btn_medium" href="agregar-clientes.php">Agregar nuevo cliente</a>
			
			</div>
			
		</div><!-- #main -->


		<footer>
		</footer><!-- /footer -->
	</div><!-- /#wrapper -->
	 
	 <script>
	 $(document).ready(function(){
		 
$( "#pista" ).keyup(function() {
  
  //Conultar
  $.get( "json/json-clientes.php",{pista:$( "#pista" ).val()}, function( data ) {
	  var contenido = '<table class="table-fill"><thead><tr><th class="text-left">Nombre</th><th class="text-left">Telefono</th></tr></thead><tbody class="table-hover">';
	    $.each(data, function( index, value ) {
			
			if($("#rol").val() == "ADMINISTRADOR"){
				contenido = contenido + '<tr><td class="text-left"><a href="agregar-clientes.php?&nombres=' + value[0] + '&apellidos=' + value[1] + '&rif=' + value[2] + '&direccion_fiscal=' + value[3] + '&direccion_entrega=' + value[4] + '&telefono_movil=' + value[5] + '&telefono_habitacion=' + value[6] + '&fax=' + value[7] + '&zona=' + value[8] + '&codigo=' + value[9] + '">' + value[0] + ' ' + value[1] + '</a> </td><td class="text-left">' + value[2] + '</td></tr>';	
			}else{
				contenido = contenido + '<tr><td class="text-left"><a href="agregar-clientes.php?&nombres=' + value[0] + '&apellidos=' + value[1] + '&rif=' + value[2] + '&direccion_fiscal=' + value[3] + '&direccion_entrega=' + value[4] + '&telefono_movil=' + value[5] + '&telefono_habitacion=' + value[6] + '&fax=' + value[7] + '&zona=' + value[8] + '&codigo=' + value[9] + '">' + value[0] + ' ' + value[1] + '</a>    <a href="asignar-cliente.php?cliente=' + value[0] + ' ' + value[1] + '&codigo=' + value[9] + '">(Vender)</a></td><td class="text-left">' + value[2] + '</td></tr>';	
				
			}
			
		  
		});
	  $("#contenido").html(contenido + '</tbody></table>');
	 },"json")

	 
})

	 
})
	 </script>
	 
	 
   </body>

 </html>