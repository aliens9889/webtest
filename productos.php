<?php
session_start();
//ini_set('memory_limit', '512M');
if(isset($_SESSION["usuario"]) == false){
	header("Location:index.php");	
}else if($_SESSION["codigo_cliente"] == ""){
	header("Location:clientes.php");	
}
?>
<!doctype html>
 <html>

   <head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
        
	  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	  <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale=1.0"/>
	  <!--Site Properties-->
	  <title>Productos</title>
	  <!-- css -->
		
		<link rel="stylesheet" href="assets/css/base-cliente.css" />
		<link rel="stylesheet" href="assets/css/menu-cliente.css" />
		<link rel="stylesheet" href="components/simpleGrid/simple-grid.min.css" />
		
		<link rel="stylesheet" href="assets/css/input.css" />
		<link rel="stylesheet" href="assets/css/buttons.css" />
		
		<!-- js -->
		<script src="assets/js/jquery-1.9.1.min.js"></script>
		<script src="assets/js/modernizr.custom.js"></script>
		
		<script src="assets/js/main.js"></script>
	  <!--Site Properties-->      
   </head>
   
   <body rol="<?php print $_SESSION["rol"]; ?>">
	 
	 <div id="wrapper">

		<?php
//		ini_set('memory_limit', '512M');
if($_SESSION["rol"] == "ADMINISTRADOR"){
			require "menu_administrador.php"; 
		}
				
		if($_SESSION["rol"] == "VENDEDOR"){
			require "menu_vendedor.php"; 
			
			print '<div style="width:50px; min-height:50px; position:fixed; left:5px; top:100px; background:#fff; padding:5px; text-align:center;">
					<a href="carrito.php">
					<img src="assets/img/Carrito de compras-48.png" width="40px" />
					<label id="cantidad_producto_carro" style="font-size:1.7em;"><?php 
					$da = json_decode($_SESSION["carrito"]);	
					$i = 0;
					foreach ($da as &$valor) {
						$i += $valor[3];
					}
					print $i;
					?></label></a>
					</div>';
			
		}
		?>	
			
<div id="main" style="padding:10px;">

<section style="max-width:950px; margin:0 auto;">

<?php 

if($_SESSION["cliente"] != "null"){

?>
<h1>Vender a 
<?php	
print $_SESSION["cliente"];	
}else{
?>
<h1>Productos 
<?php		
}

 ?></h1>

			<br>
			<input type="text" id="pista" name="pista" placeholder="Que quieres buscar?" />
			
			</h1>
					
			<br>

<?php 
			if($_SESSION["rol"] == "ADMINISTRADOR"){
				print '<a id="entrar" class="ff_btn btn_blue btn_medium" href="agregar-producto.php">Agregar nuevo producto</a>			';
			}
								
			if($_SESSION["rol"] == "VENDEDOR"){
				
			}
?>

			<div id="contenido"></div>
			
			<div style="clear: both"></div>
			
			</section>
			
		</div><!-- #main -->



		<footer>
		</footer><!-- /footer -->
	</div><!-- /#wrapper -->
	 
	 <script>
	 
	 var dominio = "http://www.ferrepacifico.com.ve/";
	 
	 $(document).ready(function(){
		 $('.modificar-productos').click(function(e){
		 
			$.post( "modulos/modificar-existencia.php", { codigo: $(this).attr("id"), existencia: $("#txt_" + $(this).attr("id")).val() }, function( data ) {
				console.log(data);
				alert("Existencia modificada");
			})
			 
		 })		 
		 
		 
		 $( "body" ).delegate( ".agregar_al_carro", "click", function() {
			
			var cantidad = prompt("Introduzca la cantidad");
			// Pedimos confirmación
			if (cantidad > 0) {
				var res = this.id.split("@");
				var codigo = res[0];
				var nombre = res[1];
				var precio = res[2];
				
				$.post( "modulos/agregar-carrito.php", { id: codigo, nombre: nombre, precio: precio,cantidad:cantidad }, function( data ) {
					$("#cantidad_producto_carro").html(parseInt($("#cantidad_producto_carro").html()) + parseInt(cantidad))
				})
			}
			
			
			
			
		});
		 
		 
		 
		 $( "#pista" ).keyup(function() {
			//Conultar
			$.get( "json/json-productos.php",{pista:$( "#pista" ).val()}, function( data ) {
			  var contenido = '';
			  $("#contenido").html("");
				$.each(data, function( index, value ) {
					//a href="agregar-producto.php?codigo='+value.codigo+'&nombre='+value.nombre+'&precio='+value.precio+'&existencia='+value.existencia+'&unidad='+value.unidad+'&descripcion='+value.descripcion+'&imagen='+value.imagen+'" style="color:#0D47A1;">
					if($("body").attr("rol") == "VENDEDOR"){
						$("#contenido").append('<div  id="padre_'+value.codigo+'" style=" border-bottom:3px solid #000; padding-bottom:10px;"><div style="font-size:25px; color:#303F9F;">'+value.nombre+'</div><table style="width:100%;"><td width="100px"><img src="' + dominio + 'images/'+value.imagen+'" width="100px" height="100px" /><br><div class="controles"><button class="agregar_al_carro" id="'+value.codigo+'@'+value.nombre+'@'+value.precio+'" style="width:100%;">Comprar</button></div></td><td><div style="padding:5px; font-size:14px;"><table><tr><td style="vertical-align:bottom;"><b>Codigo:</b></td><td>'+value.codigo_referencia+'</td></tr><tr><td style="vertical-align:bottom;"><b>Existe:</b></td><td>'+value.existencia+'</td></tr><tr><td style="vertical-align:bottom;"><b>Unidad:</b></td><td>'+value.unidad+'</td></tr><tr><td style="vertical-align:bottom;"><b>Descri:</b></td><td>'+value.descripcion+'</td></tr><tr><td style="vertical-align:bottom;"><b>Precio:</b></td><td>'+value.precio+'</td></tr></table></div></td></table></div>');
					}else{
						$("#contenido").append('<div  id="padre_'+value.codigo+'" style=" border-bottom:3px solid #000; padding-bottom:10px;"><div style="font-size:25px; color:#303F9F;"><input type="text" id="'+value.codigo+'_nombre" class="nombre" style="padding:0px; border:0px; font-size:1em;" value="'+value.nombre+'"></div><table style="width:100%;"><td width="100px"><img src="' + dominio + 'images/'+value.imagen+'" width="100px" height="100px" /><br><div class="controles"><button class="editar" id="'+value.codigo+'" style="width:50%;">E</button><button class="eliminar" id="'+value.codigo+'" style="width:50%;">X</button></div></td><td><div style="padding:5px; font-size:14px;"><table><tr><td style="vertical-align:bottom;"><b>Codigo:</b></td><td><input type="text" id="'+value.codigo+'_codigo_referencia" class="codigo_referencia" style="padding:0px; border:0px; font-size:1em;" value="'+value.codigo_referencia+'"></td></tr><tr><td style="vertical-align:bottom;"><b>Existe:</b></td><td><input type="text" class="existencia" id="'+value.codigo+'_existencia"  style="padding:0px; border:0px; font-size:1em;" value="'+value.existencia+'"></td></tr><tr><td style="vertical-align:bottom;"><b>Unidad:</b></td><td><input type="text" class="existencia"  style="padding:0px; border:0px; font-size:1em;" id="'+value.codigo+'_unidad" value="'+value.unidad+'"></td></tr><tr><td style="vertical-align:bottom;"><b>Descri:</b></td><td><input type="text" class="existencia"  style="padding:0px; border:0px; font-size:1em;" id="'+value.codigo+'_descripcion" value="'+value.descripcion+'"></td></tr><tr><td style="vertical-align:bottom;"><b>Precio:</b></td><td><input type="text" class="existencia"  style="padding:0px; border:0px; font-size:1em;" id="'+value.codigo+'_precio" value="'+value.precio+'"></td></tr></table></div></td></table></div>');
					}
					
					
				});
				
			 },"json")
		})
		
		
		$( "body" ).delegate( ".eliminar", "click", function() {
			var id = this.id;
			$.post( "modulos/eliminar-producto.php",{codigo:this.id}, function( data ) {
					if(data == 200){
						alert("Producto eliminado");		
						$("#padre_" + id).remove();					
					}
			})
		});
		
		$( "body" ).delegate( ".editar", "click", function() {
			
			var nombre = $("#"+this.id+"_nombre").val();
			var codigo_referencia = $("#"+this.id+"_codigo_referencia").val();
			var precio = $("#"+this.id+"_precio").val();
			var existencia = $("#"+this.id+"_existencia").val();
			var unidad = $("#"+this.id+"_unidad").val();
			var descripcion = $("#"+this.id+"_descripcion").val();
			
			
			$.post( "modulos/modificar-producto.php",{codigo:this.id,
			codigo_referencia:codigo_referencia,
			nombre_producto:nombre,
			precio_producto:precio,
			cantidad_producto:existencia,
			unidad_producto:unidad,
			descripcion_producto:descripcion}, function( data ) {
				
				console.log(data)
				
				if(data == 200){
					alert("Producto Modificado");					
				}
			})
			
		});
		
		
				 
		 
	 })
	 

	 
	 </script>
	 
   </body>

 </html>