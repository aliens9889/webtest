
<?php 
require_once "globales.php";	
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$sql = "SELECT usuarios.codigo,usuarios.nombre,vendedores.nombres,vendedores.apellidos,vendedores.telefono FROM vendedores,usuarios WHERE vendedores.codigo_usuario LIKE usuarios.codigo";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
	
	print '<table class="table-fill">
<thead>
<tr>
<th class="text-left">Nombre</th>
<th class="text-left">Telefono</th>
</tr>
</thead>
<tbody class="table-hover">';
	
    while($row = $result->fetch_assoc()) {
        echo '<tr>
<td class="text-left"><a href="agregar-vendedor.php?codigo='.$row["codigo"].'&nombres='.$row["nombres"].'&apellidos='.$row["apellidos"].'&telefono='.$row["telefono"].'&usuario='.$row["nombre"].'">'.$row["nombres"].' '.$row["apellidos"].'</a></td>
<td class="text-left">'.$row["telefono"].'</td>
</tr>';
    }
	
	print '</tbody>
</table>';
} else {
    echo "No tienes vendedores registrados </br>";
}
$conn->close();

?>
