<?php 
session_start();
require_once "globales.php";	
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

//nombre_vendedor
$sql = "SELECT ordenes_compras.fecha,clientes.nombres as nombre_cliente,clientes.apellidos as apellido_cliente,clientes.rif,ordenes_compras.codigo,vendedores.nombres as nombre_vendedor FROM ordenes_compras,vendedores,clientes WHERE ordenes_compras.codigo_vendedor LIKE vendedores.codigo_usuario AND ordenes_compras.codigo_cliente LIKE clientes.codigo AND ordenes_compras.codigo_vendedor LIKE '".$_SESSION["codigo"]."' ORDER BY fecha ASC";

if($_SESSION["rol"] == "ADMINISTRADOR"){
$sql = "SELECT ordenes_compras.fecha,clientes.nombres as nombre_cliente,clientes.apellidos as apellido_cliente,clientes.rif,ordenes_compras.codigo,vendedores.nombres as nombre_vendedor FROM ordenes_compras,vendedores,clientes WHERE ordenes_compras.codigo_vendedor LIKE vendedores.codigo_usuario AND ordenes_compras.codigo_cliente LIKE clientes.codigo ORDER BY fecha ASC";
}


$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    $i = 1;
    while($row = $result->fetch_assoc()) {
    
        echo '<div class="row" style="background:#ECEFF1; padding:10px; margin-top:10px;">
        
       <div class="col-12"> <h2>Orden Nº '.$i.' - '.$row["fecha"].'</h2> </div>
        
								<div class="col-12">
									 <h3>'.$row["nombre_cliente"].' '.$row["apellido_cliente"].'</h3>
									 <h4>'.$row["rif"].'</h4>
								</div>';

		$total = 0;
								
		$sql2 = "SELECT * FROM detalles_ordenes_compra WHERE codigo_orden LIKE '".$row["codigo"]."'";
		$result2 = $conn->query($sql2);
		if ($result2->num_rows > 0) {
$j = 1;
		while($row2 = $result2->fetch_assoc()) {
			
			print '<div class="col-7">' . $j . '. <b>' .$row2["producto"].'</b></div>
			<div class="col-5">
			Cantidad: '.(int)$row2["cantidad"].' Costo: '.(float)$row2["costo"].' </br> Sub Total: '.((int)$row2["cantidad"] * (float)$row2["costo"]).'
			</div>';
					
			$total = (float)$total + ((int)$row2["cantidad"] * (float)$row2["costo"]);
		$j++;	
		}		

		}		
								
								echo '
								<div class="col-12">
								<b><b> Monto total: </b>'.$total.' Bsf. </br> Vendedor: </b> '.$row["nombre_vendedor"].' 
								</div>
								
<div class="row">
<div class="col-12">
<a href="modulos/eliminar-orden-compra.php?codigo='.$row["codigo"].'">Eliminar</a>
</div>
								
								
								</div></div>';
								$i++;
    }
	
} else {
    echo "No tienes ordenes registrados </br> ";
}
$conn->close();

?>
