<?php 
require_once "globales.php";	
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$sql = "SELECT codigo,nombres,apellidos,rif,direccion_fiscal,direccion_entrega,telefono_movil,telefono_habitacion,fax,zona FROM clientes";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
	
	print '<table class="table-fill">
<thead>
<tr>
<th class="text-left">Nombre</th>
<th class="text-left">Rif</th>
</tr>
</thead>
<tbody class="table-hover">';
	
    while($row = $result->fetch_assoc()) {
        echo '<tr>
<td class="text-left"><a href="productos.php">'.$row["nombres"].' '.$row["apellidos"].'</a> <a class="ff_btn btn_blue btn_medium" href="agregar-clientes.php?codigo='.$row["codigo"].'&nombres='.$row["nombres"].'&apellidos='.$row["apellidos"].'&rif='.$row["rif"].'&direccion_fiscal='.$row["direccion_fiscal"].'&direccion_entrega='.$row["direccion_entrega"].'&telefono_movil='.$row["telefono_movil"].'&telefono_habitacion='.$row["telefono_habitacion"].'&fax='.$row["fax"].'&zona='.$row["zona"].'">Modificar</a></td>
<td class="text-left">'.$row["rif"].'</td>
</tr>';
    }
	
	print '</tbody></table>';
} else {
    echo "No tienes clientes registrados </br>";
}
$conn->close();

?>
