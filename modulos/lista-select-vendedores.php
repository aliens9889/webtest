
<?php 
require_once "globales.php";	
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$sql = "SELECT * FROM vendedores";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {										
		print '<option value="'.$row["id"].'">'.$row["nombres"].'</option>';
    }
} else {
    echo "<options value='NULL'>No tienes vendedores registrados.</options>";
}
$conn->close();

?>
