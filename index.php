<!doctype html>
 <html>

   <head>
      <meta charset="utf-8"/>  
	  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	  <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale=1.0"/>
	  <!--Site Properties-->
	  <title>Identificarse</title>
	  <!-- css -->
		
		<link rel="stylesheet" href="assets/css/base-principal.css" />
		<link rel="stylesheet" href="assets/css/menu-principal.css" />
		<link rel="stylesheet" href="components/simpleGrid/simple-grid.min.css" />
		
		<link rel="stylesheet" href="assets/css/input.css" />
		<link rel="stylesheet" href="assets/css/buttons.css" />
		
			
		<!-- js -->
		<script src="assets/js/jquery-1.9.1.min.js"></script>
		<script src="assets/js/modernizr.custom.js"></script>
	  <!--Site Properties-->      
   </head>

   <style>
   
    
	
	.profile-img
{
    width: 96px;
    height: 96px;
    margin: 0 auto 10px;
    display: block;
    -moz-border-radius: 50%;
    -webkit-border-radius: 50%;
    border-radius: 50%;
}

.info, .success, .warning, .error, .validation {
padding:20px;
margin-top:-13px;
}

.error {
color: #D8000C;
background-color: #FFBABA;
}
	
   </style>
   
   <script>
    $(document).ready(function(){
        $("#nav-mobile").html($("#nav-main").html());
		
		
        $("#nav-trigger div").click(function(){
		
			if ($("nav#nav-mobile").hasClass("expanded")) {
				$("nav#nav-mobile").removeClass("expanded");

				$("#nav-mobile").css("display","none");
				$("nav#nav-mobile ul").css("display","none");
							
			}else{
				$("nav#nav-mobile").addClass("expanded");

				$("#nav-mobile").css("display","block");
				$("nav#nav-mobile ul").css("display","block");
			}

        });
		
		var mediaquery = window.matchMedia("(max-width: 900px)");
			
		function handleOrientationChange(mediaquery) {
		  if (mediaquery.matches) {
			
		  } else {
			// mediaquery fuera de 900 - No Movile
			//alert("mediaquery no es 900");
			$("nav#nav-mobile").removeClass("expanded");
			$("#nav-mobile").css("display","none");
			$("nav#nav-mobile ul").css("display","none");
		  }
		}
		mediaquery.addListener(handleOrientationChange);
		
		
    });
	</script>
   
   <body>
	 
	 <div id="wrapper">
			
<div id="main">
			
		<div class="container">		

		<?php 
		
		$rol = "VENDEDOR";
		iF(isset($_GET)){
			$rol = $_GET["rol"];			
		}
		
		?>
		
		
		<section style="max-width:400px; padding:10px; margin:0 auto; text-align:center;">
		<form id="login" action="modulos/identificar.php" method="POST" >
		<img src="assets/img/logo_x1.jpg" width="150px" />
		</br>
		<h2>Inicia Sesión en tu cuenta como</h2>
		<h3><?php 
		if($rol != ""){
			print $rol;	
		}else{
			print "VENDEDOR"; 				
		}		
		?></h3>
		</br>
		<img src="assets/img/profile-x1.png" class="profile-img" />
		
		<div class="row" style="text-align:center;" class="Glow">
		
		<input type="text" name ="usuario" placeholder="Introduce tu usuario">
		
		<input type="password" name="contrasena" placeholder="Contraseña">
		
		<br>
		<?php 
		if(isset($_GET["error"])){
			if($_GET["error"] == "identificacion"){
				print "<div class='error'>Usuario o contraseña incorrectos</div>";
			}
		}
		?>
		
		
		
		<input type="hidden" name="rol" value ="
		<?php 
		if($rol != ""){
			print $rol;	
		}else{
			print "VENDEDOR"; 				
		}		
		?>" /> 
		
		<button type="submit" id="entrar" class="ff_btn btn_blue btn_medium" href="#">Entrar</button>
		</div>		
		</form>
		</section>
	
		</div>
			
		</div><!-- #main -->


		<footer>
		</footer><!-- /footer -->
	</div><!-- /#wrapper -->
	 
	 <script>
	 $(document).ready(function(){
		
		$("#entrar").click(function(e){
			e.preventDefault();
			e.stopPropagation();
			
			$("#login").submit();
		})
		
	 
	 })
	 </script>
	 
   </body>

 </html>