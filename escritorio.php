<?php 
session_start(); 
if(isset($_SESSION["usuario"]) == false){
	header("Location:index.php");	
}
?>
<!doctype html>
 <html>

   <head>
      <meta charset="utf-8"/>  
	  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	  <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale=1.0"/>
	  <!--Site Properties-->
	  <title>iDents</title>
	  <!-- css -->
		
		<link rel="stylesheet" href="assets/css/base-cliente.css" />
		<link rel="stylesheet" href="assets/css/menu-cliente.css" />
		<link rel="stylesheet" href="components/simpleGrid/simple-grid.min.css" />
		
			
		<!-- js -->
		<script src="assets/js/jquery-1.9.1.min.js"></script>
		<script src="assets/js/modernizr.custom.js"></script>
		
		<script src="assets/js/main.js"></script>
		
	  <!--Site Properties-->      
   </head>

   <style>
   .panel_escritorio{
	   padding:20px;	   
   }
   
   .panel_escritorio h2{
	   color:#fff;   
   }
   
   
   .panel_indigo{
	   background:#3F51B5;
       color:#fff;	   
   }
   .panel_blue{
	   background:#2196F3;
       color:#fff;	   
   }
   .panel_green{
	   background:#009688;
       color:#fff;	   
   }
   </style>
   
 
   
   <body>
	 
	 <div id="wrapper">

		<?php 
		
		if($_SESSION["rol"] == "ADMINISTRADOR"){
			require "menu_administrador.php"; 
		}
				
		if($_SESSION["rol"] == "VENDEDOR"){
			require "menu_vendedor.php"; 
		}
		
		
		
		?>	
			
<div id="main">
			
		<div class="container">		
				
					<h1>Escritorio Principal</h1>
					<h3>Bienvenido: <?php print $_SESSION["usuario"];?></h3>
			<br>
			
			<?php 
			
			
			
			
			
				if($_SESSION["rol"] == "ADMINISTRADOR"){
						require_once "panel_administrador.php";
				}
				
				if($_SESSION["rol"] == "VENDEDOR"){
						require_once "panel_vendedor.php";
				}
				
			?>	
			
			</br>
			
			<?php 
			
			if($_SESSION["rol"] == "ADMINISTRADOR"){
				print '<h1>Configuración</h1>
			</br>
		
			<div class="row">
							<div class="col-6">
								<h2>ID API</h2>
								</br>
								<h3>294689900663589</h3>
							</div>
							<div class="col-6">
								<h2>CODIGO SECRETO API</h2>
								</br>
								<h3>EOrSeClFEogizpNpXhNZ</h3>
							</div>
				</div>';
			}
					
			if($_SESSION["rol"] == "VENDEDOR"){
				
			}
			
			?>
			
			
			
		
			
			
			<br>
				
			
			</div>
			
		</div><!-- #main -->


		<footer>
		</footer><!-- /footer -->
	</div><!-- /#wrapper -->
	 
   </body>

 </html>