<?php 
session_start(); 
if(isset($_SESSION["usuario"]) == false){
	header("Location:index.php");	
}
?>
<!doctype html>
 <html>

   <head>
      <meta charset="utf-8"/>  
	  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	  <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale=1.0"/>
	  <!--Site Properties-->
	  <title>Vendedores</title>
	  <!-- css -->
		
		<link rel="stylesheet" href="assets/css/base-cliente.css" />
		<link rel="stylesheet" href="assets/css/menu-cliente.css" />
		<link rel="stylesheet" href="components/simpleGrid/simple-grid.min.css" />
		
		<link rel="stylesheet" href="components/table/cyrfB.css" />
		<link rel="stylesheet" href="assets/css/input.css" />
		<link rel="stylesheet" href="assets/css/buttons.css" />
		
		<!-- js -->
		<script src="assets/js/jquery-1.9.1.min.js"></script>
		<script src="assets/js/modernizr.custom.js"></script>
		
		<script src="assets/js/main.js"></script>
	  <!--Site Properties-->      
   </head>
   
   <body>
	 
	 <div id="wrapper">

		<?php 
if($_SESSION["rol"] == "ADMINISTRADOR"){
			require "menu_administrador.php"; 
		}
				
		if($_SESSION["rol"] == "VENDEDOR"){
			require "menu_vendedor.php"; 
		}

		?>	
			
<div id="main">
			
		<div class="container">		
				
				
				<?php 
				
				$codigo = "";
				$nombres = "";
				$apellidos = "";
				$telefono = "";
				$usuario = "";
				
				
				if(isset($_GET)){
					$codigo = $_GET["codigo"];
					$nombres = $_GET["nombres"];
					$apellidos = $_GET["apellidos"];
					$telefono = $_GET["telefono"];
					$usuario = $_GET["usuario"];
				}
				
				
				?>
				
				
				
		<form id="formulario" action="modulos/agregar-vendedor.php" method="POST">
			
					<h1>Agregar nuevo vendedor</h1>
			
			<br>	<br>
			<h3>Información de cuenta</h3>
					<div class="row">
					<input type="hidden" name="codigo" value="<?php echo $codigo; ?>">
					
							<div class="col-6">
							  <input type="text" name ="usuario" value="<?php echo $usuario; ?>" placeholder="Usuario">
							</div>
							<div class="col-6">
							  <input type="text" name ="contrasena" placeholder="Contraseña">
							</div>
					</div>
					<h3>Información de perfil</h3>
					<div class="row">
							<div class="col-6">
							  <input type="text" name ="nombres" value="<?php echo $nombres; ?>" placeholder="Nombres">
							</div>
							<div class="col-6">
							  <input type="text" name ="apellidos" value="<?php echo $apellidos; ?>" placeholder="Apellidos">
							</div>
							
							<div class="col-6">
							  <input type="text" name ="telefono"  value="<?php echo $telefono; ?>" placeholder="Telefono">
							</div>
							<div class="col-6">
							  <input type="text" name ="zona"  value="<?php echo $zona; ?>" placeholder="Zona">
							</div>
					</div>
			

</br>			

<a id="entrar" class="ff_btn btn_blue btn_medium" href="#">Guardar</a>

<?php  
if(isset($_GET["codigo"])){
	print '<a id="eliminar" class="ff_btn btn_red btn_medium" href="modulos/eliminar-vendedor.php?codigo='.$_GET["codigo"].'">Eliminar</a>';
}
?>


			</form>
			</div>
			
		</div><!-- #main -->


		<footer>
		</footer><!-- /footer -->
	</div><!-- /#wrapper -->
	 
	 <script>
	 $(document).ready(function(a){
	 
		 $("#entrar").click(function(a){
			 a.preventDefault();
			 a.stopPropagation();	 			 
			 
			 $("#formulario").submit();
			 
		 })
		 	 
		 
	 })
	 </script>
	 
   </body>

 </html>