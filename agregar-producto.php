<?php 
session_start(); 
if(isset($_SESSION["usuario"]) == false){
	header("Location:index.php");	
}
?>
<!doctype html>
 <html>

   <head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
        
	  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	  <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale=1.0"/>
	  <!--Site Properties-->
	  <title>Agregar producto</title>
	  <!-- css -->
		
		<link rel="stylesheet" href="assets/css/base-cliente.css" />
		<link rel="stylesheet" href="assets/css/menu-cliente.css" />
		<link rel="stylesheet" href="components/simpleGrid/simple-grid.min.css" />
		
		<link rel="stylesheet" href="components/table/cyrfB.css" />
		<link rel="stylesheet" href="assets/css/input.css" />
		<link rel="stylesheet" href="assets/css/buttons.css" />
		
		<!-- js -->
		<script src="assets/js/jquery-1.9.1.min.js"></script>
		<script src="assets/js/modernizr.custom.js"></script>
		
		<script src="assets/js/main.js"></script>
	  <!--Site Properties-->      
   </head>
   
   <body>
	 
	 <div id="wrapper">

		<?php 

if($_SESSION["rol"] == "ADMINISTRADOR"){
			require "menu_administrador.php"; 
		}
				
		if($_SESSION["rol"] == "VENDEDOR"){
			require "menu_vendedor.php"; 
		}
		
		?>	
			
<div id="main">
			
		<div class="container">						
				
		<form id="formulario" action="modulos/agregar-producto.php" method="POST"  enctype="multipart/form-data" >
			
					<h1>Agregar nuevo producto</h1>
			
			<br>
			
			
			<input type="file" name="image" id="image" style="visibility: hidden;">	
			
					<div class="row">
					
					
							<div class="col-4" id="photo">
							<?php 
							
							$codigo = "";
							$nombre="";
							$precio="";
							$cantidad="";
							$unidad="";
							$descripcion="";
							$imagen = "";
							if($_GET){
								$codigo = $_GET["codigo"];
								$nombre=$_GET["nombre"];
								$precio=$_GET["precio"];
								$cantidad=$_GET["existencia"];
								$unidad=$_GET["unidad"];
								$descripcion=$_GET["descripcion"];	
								$imagen = $_GET["imagen"];
								//http://www.ferrepacifico.com.ve/images/							
							}
							
							
							if($imagen != ""){
								//Tiene una imagen
								if(file_exists('images/'.$imagen)){
									print '<img  src="images/'.$imagen.'" width="100%" />';	
								}else{
									print '<img  src="assets/img/image.png" width="100%" />';										
								}
								
							}else{
								//No tiene una imagen
								print '<img  src="assets/img/image.png" width="100%" />';
							}
							
							?>													
							</div>
							<div class="col-8">
							<input type="hidden" name="codigo" value="<?php echo $codigo; ?>">
							<div class="row">
								
								<div class="col-6">
								 <input type="text" name ="nombre_producto" value="<?php print $nombre; ?>" placeholder="Nombre">
								</div>
								<div class="col-6">
								 <input type="text" name ="precio_producto" value="<?php print $precio; ?>" placeholder="Precio">
								</div>
								
								<div class="col-6">
								 <input type="text" name ="cantidad_producto" value="<?php print $cantidad; ?>" placeholder="Cantidad">
								</div>
								<div class="col-6">
								 <input type="text" name ="unidad_producto" value="<?php print $unidad; ?>" placeholder="Unidad">
								</div>
								
								<div class="col-12">
								 <input type="text" name ="descripcion_producto" value="<?php print $descripcion; ?>" placeholder="Descripción">
								</div>
								
							</div>
							
							</div>
					</div>	
</br>			

<a id="guardar" class="ff_btn btn_blue btn_medium" href="#">Guardar</a>
<?php  
if(isset($_GET["codigo"])){
	print '<a id="eliminar" class="ff_btn btn_red btn_medium" href="modulos/eliminar-producto.php?codigo='.$_GET["codigo"].'">Eliminar</a>';
}
?>


			</form>
			</div>
			
		</div><!-- #main -->


		<footer>
		</footer><!-- /footer -->
	</div><!-- /#wrapper -->
	 
	 <script>
	 $(document).ready(function(a){
	 
		 $("#guardar").click(function(a){
			 a.preventDefault();
			 a.stopPropagation();	 			 
			 
			 $("#formulario").submit();
			 
		 });
		 
		 
		 $("#photo").click(function(){
			$("#image").click();			
		});
		 
		$("#image").on('change', function() {
			//Get count of selected files
			alert
			
          var countFiles = $(this)[0].files.length;
          var imgPath = $(this)[0].value;
          var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
          var image_holder = $("#photo");
          image_holder.empty();
          if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
            if (typeof(FileReader) != "undefined") {
              //loop for each file selected for uploaded.
              for (var i = 0; i < countFiles; i++) 
              {
                var reader = new FileReader();
                reader.onload = function(e) {
                  $("<img />", {
                    "src": e.target.result,
                    "class": "thumb-image",
					"width":"200px"
                  }).appendTo(image_holder);
                }
                image_holder.show();
                reader.readAsDataURL($(this)[0].files[i]);
              }
            } else {
              alert("This browser does not support FileReader.");
            }
          } else {
            alert("Pls select only images");
          }
        }); 	 
		 
	 })
	 </script>
	 
   </body>

 </html>