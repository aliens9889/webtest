<?php 
session_start(); 
if(isset($_SESSION["usuario"]) == false){
	header("Location:index.php");	
}
?>
<!doctype html>
 <html>

   <head>
      <meta charset="utf-8"/>  
	  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	  <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale=1.0"/>
	  <!--Site Properties-->
	  <title>Vendedores</title>
	  <!-- css -->
		
		<link rel="stylesheet" href="assets/css/base-cliente.css" />
		<link rel="stylesheet" href="assets/css/menu-cliente.css" />
		<link rel="stylesheet" href="components/simpleGrid/simple-grid.min.css" />
		
		<link rel="stylesheet" href="components/table/cyrfB.css" />
		<link rel="stylesheet" href="assets/css/input.css" />
		<link rel="stylesheet" href="assets/css/buttons.css" />
		
		<!-- js -->
		<script src="assets/js/jquery-1.9.1.min.js"></script>
		<script src="assets/js/modernizr.custom.js"></script>
		
		<script src="assets/js/main.js"></script>
	  <!--Site Properties-->      
   </head>
   
   <body>
	 
	 <div id="wrapper">

		<?php 
if($_SESSION["rol"] == "ADMINISTRADOR"){
			require "menu_administrador.php"; 
		}
				
		if($_SESSION["rol"] == "VENDEDOR"){
			require "menu_vendedor.php"; 
		}
		?>	
			
<div id="main">
			
		<div class="container">		
				
		
			
					<h1>Vendedores</h1>
			
			<br>
			
						
<?php require_once "modulos/lista-vendedores.php"; ?>
		

</br>			

<a id="entrar" class="ff_btn btn_blue btn_medium" href="agregar-vendedor.php">Agregar nuevo vendedor</a>
			
			</div>
			
		</div><!-- #main -->


		<footer>
		</footer><!-- /footer -->
	</div><!-- /#wrapper -->
	 
   </body>

 </html>