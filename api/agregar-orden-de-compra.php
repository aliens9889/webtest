<?php 
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Max-Age: 86400');
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, Autorizacion");

session_start();
error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);

require_once "../modulos/globales.php";

$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
	die("Connection failed: " . $conn->connect_error);
} 

$codigo_usuario = $_SERVER['HTTP_AUTORIZACION'];
$has_access = false;

if ($codigo_usuario != null) {
	$sql = "SELECT codigo,nombre,contrasena,rol FROM usuarios WHERE codigo = '".$codigo_usuario."';";
	$result = $conn->query($sql);
	if ($result->num_rows == 0) {
		print 403;
		$conn->close();
	} else if ($result->fetch_assoc()["codigo"] != null) {
		$has_access = true;
	}
} else {
	print 403;
	$conn->close();
}

if ($has_access) {
	$success = true;

	$codigo = md5($_POST["codigo_cliente"].date('l jS \of F Y h:i:s A'));
	$sql = "INSERT INTO ordenes_compras (codigo,codigo_vendedor,codigo_cliente) VALUES ('".$codigo."','".$codigo_usuario."','".$_POST["codigo_cliente"]."')";	

	if ($conn->query($sql) === TRUE) {
		$da = json_decode($_POST["carrito"]);
		foreach ($da as &$valor) {
			$sql = "INSERT INTO detalles_ordenes_compra (codigo_orden,producto,cantidad,costo) VALUES ('".$codigo."','".$valor->nombre."','".(int)$valor->cantidad."','".(float)$valor->precio."')";
			
			if ($conn->query($sql) === TRUE) {
				// Cargamos la existencia
				$existencia = 0;
				$sql2 = "SELECT existencia FROM productos WHERE nombre LIKE '".$valor->nombre."'";
				$result2 = $conn->query($sql2);

				if ($result2->num_rows > 0) {
					while ($row2 = $result2->fetch_assoc()) {
						$existencia = (int) $row2["existencia"];
					}
				} else {
					$success = false;
				}

				$sql3 = "UPDATE productos SET existencia='".((int) $existencia- (int) $valor->cantidad)."' WHERE nombre = '".$valor->nombre."'";			
				if ($success) {
					if ($conn->query($sql3) === TRUE) {
						print 200;
					}
				} else {
					print 400;
				}
			} else {
				print 400;
			}			
		}
	} else {
		print 400;		
	}

	$conn->close();
}
?>