<?php 
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Max-Age: 86400');
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, Autorizacion");

session_start();
require_once "../modulos/globales.php";	

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$codigo_usuario = $_SERVER['HTTP_AUTORIZACION'];
$has_access = false;

if ($codigo_usuario == null) {
	print 403;
	$conn->close();
} else {
	$sql = "SELECT codigo,nombre,contrasena,rol FROM usuarios WHERE codigo = '".$codigo_usuario."';";
	$result = $conn->query($sql);
	if ($result->num_rows == 0) {
		print 403;
		$conn->close();
	} else if ($result->fetch_assoc()["codigo"] != null) {
		$has_access = true;
	}
}

if ($has_access) {
	// nombre_vendedor
	$sql = "SELECT \n"
    		. "	clientes.codigo AS codigo_cliente, clientes.nombres AS nombres_cliente, clientes.apellidos AS apellidos_cliente, \n"
	        . "	clientes.rif AS rif_cliente, ordenes_compras.fecha, ordenes_compras.codigo,\n"
                . "	detalles_ordenes_compra.producto, detalles_ordenes_compra.cantidad, \n"
                . "	detalles_ordenes_compra.costo\n"
                . "FROM\n"
                . "	clientes, ordenes_compras, detalles_ordenes_compra\n"
                . "WHERE\n"
                . "	ordenes_compras.codigo_vendedor = '".$codigo_usuario."'\n"
                . "AND\n"
                . "	ordenes_compras.codigo_cliente = clientes.codigo\n"
                . "AND\n"
                . "	ordenes_compras.codigo = detalles_ordenes_compra.codigo_orden";
	$result = $conn->query($sql);
	$final_result = array();
	$i = 0;
	
	while($row = $result->fetch_assoc()) {
		$final_result[$i] = array(
			"codigo_cliente" => $row["codigo_cliente"],
			"nombres_cliente" => $row["nombres_cliente"],
			"apellidos_cliente" => $row["apellidos_cliente"],
			"rif_cliente" => $row["rif_cliente"],
			"fecha" => $row["fecha"],
			"codigo" => $row["codigo"],
			"producto" => $row["producto"],
			"cantidad" => $row["cantidad"],
			"costo" => $row["costo"]
		);
		$i++;
	}
	
	print json_encode($final_result);
	$conn->close();
}

?>