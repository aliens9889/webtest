<?php
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Max-Age: 86400');
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

session_start();
require_once "../modulos/globales.php";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
if(isset($_POST)){
	$sql = "SELECT codigo,nombre,contrasena,rol FROM usuarios WHERE nombre LIKE '".$_POST["usuario"]."' AND contrasena LIKE '".$_POST["contrasena"]."'";
	$result = $conn->query($sql);

 	if ($result->num_rows > 0) {
	    // output data of each row
	    $row = $result->fetch_assoc();
	    print json_encode($row);            
	} else {
            print 403;
        }
} else {
   print 404;
}

$conn->close();
?>