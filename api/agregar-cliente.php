<?php 
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Max-Age: 86400');
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, Autorizacion");

session_start();
error_reporting(E_ALL & ~E_NOTICE);
require_once "../modulos/globales.php";	

$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
	die("Connection failed: " . $conn->connect_error);
}

$codigo_usuario = $_SERVER['HTTP_AUTORIZACION'];
$has_access = false;

if ($codigo_usuario != null) {
	$sql = "SELECT codigo,nombre,contrasena,rol FROM usuarios WHERE codigo = '".$codigo_usuario."';";
	$result = $conn->query($sql);
	if ($result->num_rows == 0) {
		print 403;
		$conn->close();
	} else if ($result->fetch_assoc()["codigo"] != null) {
		$has_access = true;
	}
} else {
	print 403;
	$conn->close();
}

if ($has_access) {
	// Insertamos la cuenta del vendedor
	$codigo = md5($_POST["rif"].date('l jS \of F Y h:i:s A'));

	$codigo_cliente = "";

	if($_POST["codigo"] != "") {
		// Modificamos la informacion de la cuenta
		$sql = "UPDATE clientes SET ";
		
		if ($_POST["nombres"] != "") {
		  $sql .= "nombres='".$_POST["nombres"]."',";
		}
		
		if ($_POST["apellidos"] != "") {
		  $sql .= "apellidos='".$_POST["apellidos"]."',";
		}
		
		
		if ($_POST["rif"] != "") {
		  $sql .= "rif='".$_POST["rif"]."',";
		}
		
		if ($_POST["direccion_fiscal"] != "") {
		  $sql .= "direccion_fiscal='".$_POST["direccion_fiscal"]."',";
		}
		
		if ($_POST["direccion_entrega"] != "") {
		  $sql .= "direccion_entrega='".$_POST["direccion_entrega"]."',";
		}
		
		if ($_POST["telefono_movil"] != "") {
		  $sql .= "telefono_movil='".$_POST["telefono_movil"]."',";
		}
		
		if ($_POST["telefono_habitacion"] != "") {
		  $sql .= "telefono_habitacion='".$_POST["telefono_habitacion"]."',";
		}
		
		if ($_POST["fax"] != "") {
		  $sql .= "fax='".$_POST["fax"]."',";
		}
		
		if ($_POST["zona"] != "") {
		  $sql .= "zona='".$_POST["zona"]."' ";
		}
		
		$sql = rtrim($sql, ",");
		
		$sql .= " WHERE codigo='".$_POST["codigo"]."'";

		if ($conn->query($sql) === TRUE) {
			print json_encode((object)array("cliente actualizado"=>$_POST["codigo"]));			
		} else {
			print 400;
		}
	} else {
		// Insertamos
		$sql = "INSERT INTO clientes (codigo,codigo_vendedor,nombres, apellidos, rif,direccion_fiscal,direccion_entrega,telefono_movil,telefono_habitacion,fax,zona)VALUES ('".$codigo."','".$codigo_usuario."','".$_POST["nombres"]."', '".$_POST["apellidos"]."', '".$_POST["rif"]."', '".$_POST["direccion_fiscal"]."', '".$_POST["direccion_entrega"]."', '".$_POST["telefono_movil"]."', '".$_POST["telefono_habitacion"]."', '".$_POST["fax"]."', '".$_POST["zona"]."')";

		if ($conn->query($sql) === TRUE) {
			print json_encode((object)array("nuevo cliente"=>$codigo));	
		} else {
			print 400;
		}
	}
	$conn->close();
}


?>