<?php
ini_set('memory_limit', '-1');
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Max-Age: 86400');
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once "../modulos/globales.php";
// error_reporting(E_ALL & ~E_NOTICE);

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$url = $_SERVER["REQUEST_URI"];
$parts = parse_url($url);
parse_str($parts['query'], $query);

$desde = $query['desde'] ? $query['desde'] : 0;
$hasta = $query['hasta'] ? $query['hasta'] : 30;

$sql = "SELECT * FROM productos LIMIT ". abs($hasta - $desde). ' OFFSET ' . $desde;

$result = $conn->query($sql);
$respuesta = array();
$i = 0;

function imageToDataURI($imageName) {
	$type = pathinfo($imageName, PATHINFO_EXTENSION);
	$data = file_get_contents('../images/'.$imageName);
	$dataURI = 'data:image/'.$type.';base64,'.base64_encode($data);
	return $dataURI;
}

if ($result->num_rows > 0) {
    // output data of each row

    while($row = $result->fetch_assoc()) {
		$respuesta[$i]["codigo"] = $row["codigo"];
		$respuesta[$i]["codigo_referencia"] = $row["codigo_referencia"];
		$respuesta[$i]["nombre"] = $row["nombre"];
		$respuesta[$i]["precio"] = $row["precio"];
		$respuesta[$i]["existencia"] = $row["existencia"];
		$respuesta[$i]["unidad"] = $row["unidad"];
		$respuesta[$i]["descripcion"] = $row["descripcion"];
		$respuesta[$i]["imagen"] = imageToDataURI($row["imagen"]);
		$i++;
    }
}
$conn->close();
print json_encode($respuesta);
?>
