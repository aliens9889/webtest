<?php 
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Max-Age: 86400');
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, Autorizacion");

session_start();  
require_once "../modulos/globales.php";
	
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
	die("Connection failed: " . $conn->connect_error);
} 

$codigo_usuario = $_SERVER['HTTP_AUTORIZACION'];
$has_access = false;

if ($codigo_usuario != null) {
	$sql = "SELECT codigo,nombre,contrasena,rol FROM usuarios WHERE codigo = '".$codigo_usuario."';";
	$result = $conn->query($sql);
	if ($result->num_rows == 0) {
		print 403;
	} else if ($result->fetch_assoc()["codigo"] != null) {
		$has_access = true;
	}
} else {
	print 403;
}

if ($has_access) {
	$sql = "DELETE FROM ordenes_compras WHERE codigo LIKE '".$_POST["codigo"]."'";
	if ($conn->query($sql) === TRUE) {
		//Insertamos el perfil del vendedor
		$sql = "DELETE FROM detalles_ordenes_compra WHERE codigo_orden LIKE '".$_POST["codigo"]."'";
		if ($conn->query($sql) === TRUE) {
			print 200;
		} else {
			print 400;
		}				
	} else {
		print 500;
	}
	$conn->close();
} else {
	print 403;
	$conn-close();	
}	
?>