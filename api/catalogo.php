<?php
ini_set('memory_limit', '512M');
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Max-Age: 86400');
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once "../modulos/globales.php";	
// error_reporting(E_ALL & ~E_NOTICE);

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

function imageToDataURI($imageName) {
	$type = pathinfo($imageName, PATHINFO_EXTENSION);	
	$data = file_get_contents('../images/'.$imageName);
	$dataURI = 'data:image/'.$type.';base64,'.base64_encode($data);
	return $dataURI;
}

$sql = "SELECT * FROM productos";

$result = $conn->query($sql);
$conn->close();

$respuesta = array();
$i = 0;

if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
		$respuesta[$i]["codigo"] = $row["codigo"];
		$respuesta[$i]["codigo_referencia"] = $row["codigo_referencia"];
		$respuesta[$i]["nombre"] = $row["nombre"];
		$respuesta[$i]["precio"] = $row["precio"];
		$respuesta[$i]["existencia"] = $row["existencia"];
		$respuesta[$i]["unidad"] = $row["unidad"];
		$respuesta[$i]["descripcion"] = $row["descripcion"];
		$respuesta[$i]["imagen"] = imageToDataURI($row["imagen"]);
		$i++;
    }
}

// $path_to_json = "/tmp/catalogo.json";
// $path_to_json = "catalogo.json";

// $file = fopen($path_to_json, "w") or die("Unable to open file!");

// fwrite($file, json_encode($string));

file_put_contents('catalogo.json', json_encode($respuesta, JSON_UNESCAPED_SLASHES));

// header("Content-Type: application/json");
// header("Content-Transfer-Encoding: Binary");
// header("Content-Length:".filesize("catalogo.json"));
// header("Content-Disposition: attachment; filename=catalogo.json");

// readfile("catalogo.json");

print json_encode(array("mensaje" => "catalogo.json creado"));

// fclose($file);
exit;
?>