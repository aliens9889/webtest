<?php 
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST, GET");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, Autorizacion, X-Requested-With");

session_start();  
require_once "globales.php";

$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
	die("Connection failed: " . $conn->connect_error);
} 

$codigo_usuario = $_SERVER['HTTP_AUTORIZACION'];
$has_access = false;

if ($codigo_usuario != null) {
	$sql = "SELECT codigo,nombre,contrasena,rol FROM usuarios WHERE codigo = '".$codigo_usuario."';";
	$result = $conn->query($sql);
	if ($result->num_rows == 0) {
		print 403;
	} else if ($result->fetch_assoc()["codigo"] != null) {
		$has_access = true;
	}
} else {
	print 403;
}

if ($has_access) {
	$codigo = md5($_POST["codigo_cliente"].date('l jS \of F Y h:i:s A'));
	print $_POST["codigo_cliente"];
	print $_POST["carrito"];
	$sql = "INSERT INTO ordenes_compras (codigo,codigo_vendedor,codigo_cliente)VALUES ('".$codigo."','".$codigo_usuario."','".$_POST["codigo_cliente"]."')";
	print $sql;
	if ($conn->query($sql) === TRUE) {
		$da = json_decode($_POST["carrito"]);	
		foreach ($da as &$valor) {
			echo $valor[1] . ' ' . $valor[2] . ' ' . $valor[3] . " = " . ($valor[2]*$valor[3]) . "</br>";
			$sql = "INSERT INTO detalles_ordenes_compra (codigo_orden,producto,cantidad,costo)VALUES ('".$codigo."','".$valor[1]."','".$valor[3]."','".$valor[2]."')";
			$conn->query($sql);	
			
	// Cargamos la existencia	
	$existencia = 0;
	$sql2 = "SELECT existencia FROM productos WHERE nombre LIKE '".$valor[1]."'";
	$result2 = $conn->query($sql2);
	if ($result2->num_rows > 0) {
	    // output data of each row
	    while($row2 = $result2->fetch_assoc()) {
			$existencia = (int)$row2["existencia"];			
		}
	}		


	$sql3 = "UPDATE productos SET existencia='".((int)$existencia-(int)$valor[3])."' WHERE nombre='".$valor[1]."'";
	$conn->query($sql3);

		}
	} else {
		echo "Error: " . $sql . "<br>" . $conn->error;
	}

	print 200;
        $conn->close();
} else {
	print 403;
	$conn->close();
}
?>