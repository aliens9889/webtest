<?php 
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Max-Age: 86400');
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, Autorizacion");

session_start();
require_once "../modulos/globales.php";	

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$codigo_usuario = $_SERVER['HTTP_AUTORIZACION'];
$has_access = false;

if ($codigo_usuario == null) {
	print 403;
	$conn->close();
} else {
	$sql = "SELECT codigo,nombre,contrasena,rol FROM usuarios WHERE codigo = '".$codigo_usuario."';";
	$result = $conn->query($sql);
	if ($result->num_rows == 0) {
		print 403;
		$conn->close();
	} else if ($result->fetch_assoc()["codigo"] != null) {
		$has_access = true;
	}
}

if ($has_access) {
	$sql = "SELECT codigo,nombres,apellidos,rif,direccion_fiscal,direccion_entrega,telefono_movil,telefono_habitacion,fax,zona FROM clientes WHERE codigo_vendedor = '".$codigo_usuario."' ORDER BY nombres ASC;";
	$result = $conn->query($sql);
	$respuesta = array();
	$i = 0;
	if ($result->num_rows > 0) {
    	// output data of each row	
	    while($row = $result->fetch_assoc()) {
	    	$respuesta[$i] = array(
	    		"nombres" => $row["nombres"],
	    		"apellidos" => $row["apellidos"],
	    		"rif" => $row["rif"],
	    		"direccion_fiscal" => $row["direccion_fiscal"],
	    		"direccion_entrega" => $row["direccion_entrega"],
	    		"telefono_movil" => $row["telefono_movil"],
	    		"telefono_habitacion" => $row["telefono_habitacion"],
	    		"fax" => $row["fax"],
	    		"zona" => $row["zona"],
	    		"codigo" => $row["codigo"]
	    	);
			$i++;
	    }

	}
	$conn->close();
	print json_encode($respuesta);
}
?>
