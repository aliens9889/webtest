<?php 
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Max-Age: 86400');
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, Autorizacion");

session_start();
require_once "../modulos/globales.php";	

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$codigo_usuario = $_SERVER['HTTP_AUTORIZACION'];
$has_access = false;

if ($codigo_usuario == null) {
	print 403;
	$conn->close();
} else {
	$sql = "SELECT codigo,nombre,contrasena,rol FROM usuarios WHERE codigo = '".$codigo_usuario."';";
	$result = $conn->query($sql);
	if ($result->num_rows == 0) {
		print 403;
		$conn->close();
	} else if ($result->fetch_assoc()["codigo"] != null) {
		$has_access = true;
	}
}

if ($has_access) {
	$sql = "SELECT cheques_devueltos.id,cheques_devueltos.fecha,cheques_devueltos.fecha_rebote,cheques_devueltos.cliente,cheques_devueltos.banco,cheques_devueltos.numero FROM cheques_devueltos,vendedores WHERE vendedores.id LIKE cheques_devueltos.codigo_vendedor AND vendedores.codigo_usuario LIKE '".$codigo_usuario."'";
	
	$result = $conn->query($sql);
	$final_result = array();
	$i = 0;
	while($row = $result->fetch_assoc()) {
		$final_result[$i] = array(
			"id" => $row["id"],
			"fecha" => $row["fecha"],
			"fecha_rebote" => $row["fecha_rebote"],
			"cliente" => $row["cliente"],
			"banco" => $row["banco"],
			"numero" => $row["numero"]
		);
		$i++;
	}
	print json_encode($final_result);
	$conn->close();
}
?>