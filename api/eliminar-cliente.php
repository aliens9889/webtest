<?php 
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Max-Age: 86400');
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With, Autorizacion");

session_start();  
require_once "../modulos/globales.php";
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
	die("Connection failed: " . $conn->connect_error);
} 

$codigo_usuario = $_SERVER['HTTP_AUTORIZACION'];
$has_access = false;

if ($codigo_usuario == null) {
	print 403;
	$conn->close();
} else {
	$sql = "SELECT codigo,nombre,contrasena,rol FROM usuarios WHERE codigo = '".$codigo_usuario."';";
	$result = $conn->query($sql);
	if ($result->num_rows == 0) {
		print 403;
		$conn->close();
	} else if ($result->fetch_assoc()["codigo"] != null) {
		$has_access = true;
	}
}

if ($has_access) {
	$sql = "DELETE FROM clientes WHERE codigo LIKE '".$_POST["codigo"]."'";
	if ($conn->query($sql) === TRUE) {
		print json_encode((object) array("cliente eliminado"=> $_POST["codigo"]));			
	} else {
		print 400;
	}
	$conn->close();
}
			
?>