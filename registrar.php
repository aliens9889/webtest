<!doctype html>
 <html>

   <head>
      <meta charset="utf-8"/>  
	  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
	  <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale=1.0"/>
	  <!--Site Properties-->
	  <title>Crear cuenta</title>
	  <!-- css -->
		
		<link rel="stylesheet" href="assets/css/base-principal.css" />
		<link rel="stylesheet" href="assets/css/menu-principal.css" />
		<link rel="stylesheet" href="components/simpleGrid/simple-grid.min.css" />
		
		<link rel="stylesheet" href="assets/css/input.css" />
		<link rel="stylesheet" href="assets/css/buttons.css" />
		
			
		<!-- js -->
		<script src="assets/js/jquery-1.9.1.min.js"></script>
		<script src="assets/js/modernizr.custom.js"></script>
	  <!--Site Properties-->      
   </head>

   <style>
   
    
	
	.profile-img
{
    width: 96px;
    height: 96px;
    margin: 0 auto 10px;
    display: block;
    -moz-border-radius: 50%;
    -webkit-border-radius: 50%;
    border-radius: 50%;
}
	
   </style>
   
   <script>
    $(document).ready(function(){
        $("#nav-mobile").html($("#nav-main").html());
		
		
        $("#nav-trigger div").click(function(){
		
			if ($("nav#nav-mobile").hasClass("expanded")) {
				$("nav#nav-mobile").removeClass("expanded");

				$("#nav-mobile").css("display","none");
				$("nav#nav-mobile ul").css("display","none");
							
			}else{
				$("nav#nav-mobile").addClass("expanded");

				$("#nav-mobile").css("display","block");
				$("nav#nav-mobile ul").css("display","block");
			}
		
            /*if ($("nav#nav-mobile ul").hasClass("expanded")) {
                $("nav#nav-mobile ul.expanded").removeClass("expanded")//.slideUp(250);
                //$(this).removeClass("open");
				
				$("nav#nav-mobile").css("visibility","hidden");
				
            } else {
                $("nav#nav-mobile ul").addClass("expanded")//.slideDown(250);
                //$(this).addClass("open");
				
				$("nav#nav-mobile").css("visibility","visible");
				
            }*/
        });
		
		var mediaquery = window.matchMedia("(max-width: 900px)");
			
		function handleOrientationChange(mediaquery) {
		  if (mediaquery.matches) {
			// mediaquery dentro de 900 - Movile
			//alert("mediaquery es 900");
			
		  } else {
			// mediaquery fuera de 900 - No Movile
			//alert("mediaquery no es 900");
			$("nav#nav-mobile").removeClass("expanded");
			$("#nav-mobile").css("display","none");
			$("nav#nav-mobile ul").css("display","none");
		  }
		}
		mediaquery.addListener(handleOrientationChange);
		
		
    });
	</script>
   
   <body>
	 
	 <div id="wrapper">
			
<div id="main">
			
		<div class="container">		

		
		<section style="max-width:400px; padding:10px; margin:0 auto; text-align:center;">
		<form action="identificar.php" method="POST" >
		<img src="assets/img/logo_x1.jpg" width="150px" />
		</br>
		<h2>Crea tu cuenta de oPlaza</h2>
		</br>
		
		<div style="text-align:center;">
		
		
			<div class="row">
							<div class="col-6">
					<input type="text" placeholder="Nombres">
							</div>
							<div class="col-6">
					<input type="text" placeholder="Apellidos">
							</div>
							
							<div class="col-12">
					<input type="text" placeholder="Nombre de usuario">
							</div>
							
							<div class="col-12">
							<input type="password" placeholder="Contraseña">
							</div>
			</div>
			<h3>Fecha de nacimiento</h3>
			<div class="row">			
							<div class="col-4">
					<input type="text" placeholder="Dia">
							</div>
							<div class="col-4">
					<input type="text" placeholder="Mes">
							</div>
							<div class="col-4">
					<input type="text" placeholder="Año">
							</div>
							
							<div class="col-12">
					<input type="text" placeholder="Telefono movil">
							</div>
							
							
			</div>		
		
		
		
		
		
		
		
		<br>
				
		<button class="ff_btn btn_blue btn_medium">Crear cuenta</button>
								
		</div>		
		</form>
		</section>
		
		
		
		</div>
			
		</div><!-- #main -->


		<footer>
		</footer><!-- /footer -->
	</div><!-- /#wrapper -->
	 
	 <script>
	 $(document).ready(function(){
		 $("#recuperar").click(function(e){
			 e.preventDefault();
			 e.stopPropagation();
			 
			 window.location="http://localhost/idents/registrar.php";
			 
		 });
	 })
	 </script>
	 
   </body>

 </html>